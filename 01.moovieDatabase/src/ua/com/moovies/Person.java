/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.moovies;

/**
 *
 * @author a_moskalenko
 */
public class Person implements ContainingTheQuery{

    private final String name;
    private final int id;

    public Person(int id, String name) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public String getQuery() {
        return "SELECT "
                + "Films.id, "
                + "Films.title, "
                + "Films.year, "
                + "Films.IMDb, "
                + "Films.editor_id, "
                + "persons.name AS editor_name "
                + "FROM "
                + "Films "
                + "INNER JOIN persons ON Films.editor_id = persons.id "
                + "WHERE editor_id = " + id;
    }

    @Override
    public String toString() {
        return name;
    }

}
