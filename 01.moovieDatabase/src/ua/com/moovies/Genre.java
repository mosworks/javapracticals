/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.moovies;

/**
 *
 * @author a_moskalenko
 */
public class Genre implements ContainingTheQuery {

    private final String name;
    private final int id;

    public Genre(int id, String name) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public String getQuery() {
        return "SELECT "
                + "Films.id, "
                + "Films.title, "
                + "Films.year, "
                + "Films.IMDb, "
                + "Films.editor_id, "
                + "persons.name AS editor_name "
                + "FROM "
                + "filmsGenres "
                + "INNER JOIN Films ON filmsGenres.film_id = Films.id AND (filmsGenres.genre_id = " 
                + id + ")"
                + "LEFT JOIN persons ON Films.editor_id = persons.id";
    }

    @Override
    public String toString() {
        return name;
    }

}
